@extends('layout.master')

@section('title', 'form')

@section('content')
        <label for="firstName">First Name</label><br>
        <input type="text" id="firstName" name="firstName"><br><br>

        <label for="lastName">Last Name</label><br>
        <input type="text" id="lastName" name="lastName"><br><br>
        <button type="submit">Submit</button>
@endsection

{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1.2 SanberCode</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/send" method="POST">
        @csrf
        <label for="firstName">First Name</label><br>
        <input type="text" id="firstName" name="firstName"><br><br>

        <label for="lastName">Last Name</label><br>
        <input type="text" id="lastName" name="lastName"><br><br>

        <label for="gender">Gender:</label><br>
        <input type="radio" id="male" name="gender" value="Male">Male<br>
        <input type="radio" id="female" name="gender" value="Female">Female<br>
        <input type="radio" id="other" name="gender" value="Other">Other<br><br>

        <label for="nationality">Nationality:</label><br>
        <select id="nationality" name="nationality">
            <option value="Indonesian">Indonesia</option>
            <option value="English">English</option>
            <option value="American">America</option>
        </select><br><br>

        <label for="language">Language Spoken:</label><br>
        <input type="checkbox" id="language" value="Bahasa Indonesia"> Bahasa Indonesia<br>
        <input type="checkbox" id="language" value="English"> English<br>
        <input type="checkbox" id="language" value="Other"> Other<br><br>

        <label for="bio">Bio:</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="send">
    </form>
</body>
</html> --}}