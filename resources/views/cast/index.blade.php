@extends('layout.master')

@section('title', 'All Cast')

@section('content')
<a href="/cast/create" class="btn btn-primary my-3">Add Cast</a>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Umur</th>
                    <th scope="col">Biografi</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($cast as $keys=>$cas)
                <tr>
                    <td scope="row">{{$keys + 1}}</td>
                    <td scope="row">{{$cas->nama}}</td>
                    <td scope="row">{{$cas->umur}}</td>
                    <td scope="row">{{$cas->bio}}</td>
                    <td>
                        <form action="/cast/{{$cas->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$cas->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$cas->id}}/edit" class="btn btn-warning btn-sm">Update</a>
                            <button type="submit" class="btn btn-danger" btn-sm>Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td scope="row">Cast Kosong!</td>
                </tr>
                @endforelse
            </tbody>
        </table>
@endsection