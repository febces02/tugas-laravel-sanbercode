@extends('layout.master')

@section('title', 'Cast Detail')

@section('content')
        <h2 class="text-primary">{{$cast->nama}}</h2>
        <h2 class="text-primary">{{$cast->umur}}</h2>
        <p>{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-primary btn-sm"></a>
@endsection