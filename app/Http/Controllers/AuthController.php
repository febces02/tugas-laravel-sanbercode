<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function send(Request $request) {
        $namaDepan = $request['firstName'];
        $namaBelakang = $request['lastName'];

        return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
