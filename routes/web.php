<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('welcome');
// });

Route::get('/welcome', [HomeController::class, 'welcome']);

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', [HomeController::class, 'home']);

// Route::get('/register', function () {
//     return view('register');
// });

Route::get('/register', [AuthController::class, 'register']);

Route::post('/send', [AuthController::class, 'send']);

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/data-table', function() {
    return view('pages.data-table');
});

Route::get('/table', function() {
    return view('pages.table');
});

//CRUD for Cast
//Create
Route::get('/cast/create', [CastController::class, 'create']);

Route::post('/cast', [CastController::class, 'store']);

//Read
Route::get('/cast', [CastController::class, 'index']);

Route::get('/cast/(id)', [CastController::class, 'show']);

//Update
Route::get('/cast/(id)/edit', [CastController::class, 'edit']);

Route::put('/cast/(id)', [CastController::class, 'update']);

//Delete
Route::delete('/cast/(id)', [CastController::class, 'destroy']);